'use strict'

let Darkchess = function() {

    let state = {};
    resetState();

    // PRIVATE FUNCTIONS:

    function resetState() {
        state.turn = Darkchess.WHITE;
        state.board = new Array(128);
        state.inView = {};
        state.inView[Darkchess.WHITE] = new Array(128);
        state.inView[Darkchess.BLACK] = new Array(128);
        state.kings = {};
        state.kings[Darkchess.WHITE] = null;
        state.kings[Darkchess.BLACK] = null;
        state.castling = {};
        state.castling[Darkchess.WHITE] = 0;
        state.castling[Darkchess.BLACK] = 0;
        state.halfMoves = 0;
        state.moveNumber = 1;
        state.history = [];
    }

    function buildMove(color, type, from, to, flags, promotion) {
        let move = {
            color: color,
            from: from,
            to: to,
            flags: flags,
            piece: type
        };

        if (promotion) {
            move.flags |= Darkchess.BITS.PROMOTION;
            move.promotion = promotion;
        }

        if (state.board[to]) {
            move.captured = state.board[to].type;
        }

        return move;
    }

    function addMove(color, moves, pieceType, from, to, flags) {
        let toBePromoted = (Darkchess.rank(to) === Darkchess.RANK_8 || Darkchess.rank(to) === Darkchess.RANK_1);
        if (pieceType === Darkchess.PAWN && toBePromoted) {
            let pieces = [Darkchess.QUEEN, Darkchess.ROOK, Darkchess.BISHOP, Darkchess.KNIGHT];
            for (let i=0; i<pieces.length; i++) {
                let move = buildMove(color, pieceType, from, to, flags, pieces[i]);
                moves.push(move);
            }
        } else {
            let move = buildMove(color, pieceType, from, to, flags);
            moves.push(move);
        }
    }

    function generateInView(playerColor) {

        let inView = [];

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {
            // Did we run off the end of the board?
            if (i & 0x88) { i += 7; continue; }
            inView[i] = false;
        }

        let opponent = Darkchess.swapColor(playerColor);
        let secondRank = {b: Darkchess.RANK_7, w: Darkchess.RANK_2};

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {
            // Did we run off the end of the board?
            if (i & 0x88) { i += 7; continue; }

            let piece = state.board[i];

            if (piece == null || piece.color !== playerColor) continue;
            inView[i] = true;

            if (piece.type === Darkchess.PAWN) {
                let square = i + Darkchess.PAWN_OFFSETS[playerColor][0];
                inView[square] = true;

                square = i + Darkchess.PAWN_OFFSETS[playerColor][1];
                if (secondRank[playerColor] === Darkchess.rank(i)) {
                    inView[square] = true;
                }

                // Handle pawn captures
                for (let offset = 2; offset < 4; offset++) {
                    let index = i + Darkchess.PAWN_OFFSETS[playerColor][offset];
                    if (index & 0x88) continue;
                    inView[index] = true;
                }
            } else {
                let numAttackOptions = Darkchess.PIECE_OFFSETS[piece.type].length;
                for (let attackIndex = 0; attackIndex < numAttackOptions; attackIndex++) {
                    let offset = Darkchess.PIECE_OFFSETS[piece.type][attackIndex];
                    let index = i;

                    while(true) {
                        index += offset;
                        if (index & 0x88) break;

                        if (state.board[index] == null) {
                            inView[index] = true;
                        } else {
                            inView[index] = true;
                            break;
                        }

                        if (piece.type === 'n' || piece.type === 'k') break;
                    }
                }
            }
        }

        return inView;
    }

    function kingCaptured() {
        if (state.kings[Darkchess.WHITE] === null) return Darkchess.WHITE;
        if (state.kings[Darkchess.BLACK] === null) return Darkchess.BLACK;
        return false;
    }

    function getPiece(square) {
        let piece = state.board[Darkchess.SQUARES[square]];
        return (piece)? { type: piece.type, color: piece.color }: null;
    }

    function storeMove(move) {
        let entry = {
            turn: state.turn,
            inView: { w: state.inView.w, b: state.inView.b },
            move: move,
            kings: { w: state.kings.w, b: state.kings.b },
            castling: { w: state.castling.w, b: state.castling.b },
            halfMoves: state.halfMoves,
            moveNumber: state.moveNumber
        };

        state.history.push(entry);
    }

    function placePieceOnBoard(piece, square) {

        let isValidPieceObject = ('type' in piece && 'color' in piece);
        if (!isValidPieceObject) return false;

        let validPieceSymbols = 'pnbrqkPNBRQK';
        let isValidPiece = (validPieceSymbols.indexOf(piece.type.toLowerCase()) > -1);
        if (!isValidPiece) return false;

        state.board[square] = { type: piece.type, color: piece.color };
        if (piece.type === Darkchess.KING) {
            state.kings[piece.color] = square;
        }

        return true;
    }

    function moveToSan(move) {
        let output = '';

        if (move.flags & Darkchess.BITS.KSIDE_CASTLE) {
            output = 'O-O';
        } else if (move.flags & Darkchess.BITS.QSIDE_CASTLE) {
            output = 'O-O-O';
        } else {

            let disambiguator = getDisambiguator(move);

            if (move.piece !== Darkchess.PAWN) {
                output += move.piece.toUpperCase() + disambiguator;
            }

            if (move.flags & Darkchess.BITS.CAPTURE) {
                if (move.piece === Darkchess.PAWN) {
                    output += Darkchess.algebraic(move.from)[0];
                }
                output += 'x';
            }

            output += Darkchess.algebraic(move.to);

            if (move.flags & Darkchess.BITS.PROMOTION) {
                output += '=' + move.promotion.toUpperCase();
            }
        }

        return output;
    }

    function getDisambiguator(move) {

        let moves = generateMoves();

        let ambiguities = 0;
        let sameRank = 0;
        let sameFile = 0;

        for (let i = 0; i < moves.length; i++) {
            let move2 = moves[i];

            let samePieceType = (move.piece === move2.piece);
            let differentFromSquare = (move.from !== move2.from);
            let sameToSquare = (move.to === move2.to);

            if (samePieceType && differentFromSquare && sameToSquare) {
                ambiguities++;

                if (Darkchess.rank(move.from) === Darkchess.rank(move2.from)) {
                    sameRank++;
                }

                if (Darkchess.file(move.from) === Darkchess.file(move2.from)) {
                    sameFile++;
                }
            }
        }

        if (ambiguities > 0) {

            // Use square as the disambiguator when moves of both rank and file exists.
            if (sameRank > 0 && sameFile > 0) {
                return Darkchess.algebraic(move.from);
            }

            // Use rank as the disambiguator when moves on the same file exists
            if (sameFile > 0) {
                return Darkchess.algebraic(move.from).charAt(1);
            }

            // Use file as the disambiguator otherwise
            return Darkchess.algebraic(move.from).charAt(0);
        }

        return '';
    }

    //  PUBLIC FUNCTIONS:

    function isValidMove(move) {
        let moves = generateMoves();

        let move0x88 = null;

        for (let i = 0; i < moves.length; ++i) {
            let currentMove = moves[i];

            let sameDestination =   (move.from === Darkchess.algebraic(currentMove.from));
            let sameTarget =        (move.to   === Darkchess.algebraic(currentMove.to));
            let hasPromotion =      ('promotion' in currentMove);
            let samePromotion =     (move.promotion === currentMove.promotion);

            if (sameDestination && sameTarget && (!hasPromotion || samePromotion)) {
                move0x88 = currentMove;
                break;
            }
        }

        let isSameSquare = (move.from === move.to);
        let piece = getPiece(move.to);
        let captureOwnPiece = (piece !== null && piece.color === state.turn);

        return (!isSameSquare && !captureOwnPiece)? move0x88: null;
    }

    function move(move) {

        storeMove(move);

        let opponent = Darkchess.swapColor(state.turn);
        let board = state.board;

        let capturedPiece = board[move.to];
        let movingPiece = board[move.from];

        // King capture
        if (move.flags & Darkchess.BITS.CAPTURE && move.captured === Darkchess.KING) {
            state.kings[capturedPiece.color] = null;
        }

        board[move.to] = movingPiece;
        board[move.from] = null;

        // Pawn promotion (replace new piece)
        if (move.flags & Darkchess.BITS.PROMOTION) {
            board[move.to] = { type: move.promotion, color: state.turn };
        }

        if (move.piece === Darkchess.KING) {
            state.kings[move.color] = move.to;

            // If we castled, move the rook next to the king
            if (move.flags & Darkchess.BITS.KSIDE_CASTLE) {
                let castlingTo = move.to - 1;
                let castlingFrom = move.to + 1;
                board[castlingTo] = board[castlingFrom];
                board[castlingFrom] = null;
            } else if (move.flags & Darkchess.BITS.QSIDE_CASTLE) {
                let castlingTo = move.to + 1;
                let castlingFrom = move.to - 2;
                board[castlingTo] = board[castlingFrom];
                board[castlingFrom] = null;
            }

            // Turn off castling
            state.castling[state.turn] = '';
        }

        // Turn off castling if we move a rook
        if (state.castling[state.turn]) {
            for (let i = 0, len = Darkchess.ROOKS[state.turn].length; i < len; i++) {
                if (move.from === Darkchess.ROOKS[state.turn][i].square && state.castling[state.turn] & Darkchess.ROOKS[state.turn][i].flag) {
                    state.castling[state.turn] ^= Darkchess.ROOKS[state.turn][i].flag;
                    break;
                }
            }
        }

        // Turn off castling if we capture a rook
        if (state.castling[opponent]) {
            for (let i = 0, len = Darkchess.ROOKS[opponent].length; i < len; i++) {
                if (move.to === Darkchess.ROOKS[opponent][i].square && state.castling[opponent] & Darkchess.ROOKS[opponent][i].flag) {
                    state.castling[opponent] ^= Darkchess.ROOKS[opponent][i].flag;
                    break;
                }
            }
        }

        // Reset the 50 move counter if pawn is moved or a piece is captured
        if (move.piece === Darkchess.PAWN) {
            state.halfMoves = 0;
        } else if (move.flags & Darkchess.BITS.CAPTURE) {
            state.halfMoves = 0;
        } else {
            state.halfMoves++;
        }

        if (state.turn === Darkchess.BLACK) {
            state.moveNumber++;
        }

        state.inView[Darkchess.WHITE] = generateInView(Darkchess.WHITE);
        state.inView[Darkchess.BLACK] = generateInView(Darkchess.BLACK);

        swapPlayer();
    }

    function undoMove() {
        let oldEntry = state.history.pop();
        if (oldEntry == null) { return; }

        let move = oldEntry.move;

        state.kings = oldEntry.kings;
        state.turn = oldEntry.turn;
        state.castling = oldEntry.castling;
        state.halfMoves = oldEntry.halfMoves;
        state.moveNumber = oldEntry.moveNumber;

        let playerColor = state.turn;
        let opponent = Darkchess.swapColor(playerColor);

        state.board[move.from] = state.board[move.to];
        state.board[move.from].type = move.piece;
        state.board[move.to] = null;

        if (move.flags & Darkchess.BITS.CAPTURE) {
            state.board[move.to] = { type: move.captured, color: opponent };
        }

        if (move.flags & (Darkchess.BITS.KSIDE_CASTLE | Darkchess.BITS.QSIDE_CASTLE)) {
            let castlingTo;
            let castlingFrom;
            if (move.flags & Darkchess.BITS.KSIDE_CASTLE) {
                castlingTo = move.to + 1;
                castlingFrom = move.to - 1;
            } else if (move.flags & Darkchess.BITS.QSIDE_CASTLE) {
                castlingTo = move.to - 2;
                castlingFrom = move.to + 1;
            }

            state.board[castlingTo] = state.board[castlingFrom];
            state.board[castlingFrom] = null;
        }

        state.inView[Darkchess.WHITE] = generateInView(Darkchess.WHITE);
        state.inView[Darkchess.BLACK] = generateInView(Darkchess.BLACK);

        return move;
    }

    function swapPlayer() {
        state.turn = Darkchess.swapColor(state.turn);
        return state.turn;
    }

    function isGameOver() {

        let halfMovesLimit = 50;
        let encounteredMoveRule = (state.halfMoves >= halfMovesLimit);
        let aKingCaptured = (kingCaptured() !== false);

        return (encounteredMoveRule || aKingCaptured || insufficientMaterial());
    }

    function inDraw() {
        return hit50MoveRule() || insufficientMaterial();
    }

    function hit50MoveRule() {
        return (state.halfMoves >= 100);
    }

    function insufficientMaterial() {
        let pieces = {};
        let bishops = [];
        let numPieces = 0;
        let squareColor = 0;

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {
            squareColor = (squareColor + 1) % 2;
            if (i % 0x88) { i += 7; continue }

            let piece = state.board[i];
            if (piece) {
                pieces[piece.type] = (piece.type in pieces) ?
                                     pieces[piece.type] + 1: 1;
                if (pieces.type === Darkchess.BISHOP) {
                    bishops.push(squareColor);
                }
                numPieces++;
            }
        }

        // king vs king
        if (numPieces === 2) {
            return true;
        }

        return false;
    }

    function setState(fen) {

        let tokens = fen.split(/\s+/);
        let piecePlacement = tokens[0];

        //  TODO(thomas): Validate fen

        resetState();

        let square = 0;
        for (let i = 0; i < piecePlacement.length; i++) {
            let piece = piecePlacement.charAt(i);
            if (piece === '/') {
                square += 8;
            } else if (Darkchess.isDigit(piece)) {
                square += parseInt(piece, 10);
            } else {
                let color = (piece < 'a')? Darkchess.WHITE: Darkchess.BLACK;
                let pieceObj = {type: piece.toLowerCase(), color: color };

                placePieceOnBoard(pieceObj, square);
                ++square;
            }
        }

        state.turn = tokens[1];

        if (tokens[2].indexOf('K') > -1) state.castling.w |= Darkchess.BITS.KSIDE_CASTLE;
        if (tokens[2].indexOf('Q') > -1) state.castling.w |= Darkchess.BITS.QSIDE_CASTLE;
        if (tokens[2].indexOf('k') > -1) state.castling.b |= Darkchess.BITS.KSIDE_CASTLE;
        if (tokens[2].indexOf('q') > -1) state.castling.b |= Darkchess.BITS.QSIDE_CASTLE;

        state.halfMoves = parseInt(tokens[3], 10);
        state.moveNumber = parseInt(tokens[4], 10);

        state.inView[Darkchess.WHITE] = generateInView(Darkchess.WHITE);
        state.inView[Darkchess.BLACK] = generateInView(Darkchess.BLACK);

        return true;
    }

    function getWinner() {
        let loser = kingCaptured();
        if (!loser) return false;
        return Darkchess.swapColor(loser);
    }

    function generateMoves(options) {

        let turnsColor = state.turn;
        let opponentColor = Darkchess.swapColor(turnsColor);
        let inViewInfo = state.inView[state.turn];

        let captureOnly = false;

        if (typeof options !== 'undefined') {
            if ('inViewInfo' in options) inViewInfo = options.inViewInfo;
            if ('captureOnly' in options) captureOnly = options.captureOnly;
        }

        let moves = [];
        let secondRank = {b: Darkchess.RANK_7, w: Darkchess.RANK_2 };

        let startIndecies = [];
        if (turnsColor === Darkchess.WHITE) {
            startIndecies[0] = Darkchess.SQUARES.a1;
            startIndecies[1] = Darkchess.SQUARES.a2;
            startIndecies[2] = Darkchess.SQUARES.a3;
            startIndecies[3] = Darkchess.SQUARES.a4;
            startIndecies[4] = Darkchess.SQUARES.a5;
            startIndecies[5] = Darkchess.SQUARES.a6;
            startIndecies[6] = Darkchess.SQUARES.a7;
            startIndecies[7] = Darkchess.SQUARES.a8;
        } else {
            startIndecies[0] = Darkchess.SQUARES.a8;
            startIndecies[1] = Darkchess.SQUARES.a7;
            startIndecies[2] = Darkchess.SQUARES.a6;
            startIndecies[3] = Darkchess.SQUARES.a5;
            startIndecies[4] = Darkchess.SQUARES.a4;
            startIndecies[5] = Darkchess.SQUARES.a3;
            startIndecies[6] = Darkchess.SQUARES.a2;
            startIndecies[7] = Darkchess.SQUARES.a1;
        }

        for (let i = 0; i < 8; i++) {
            let startIndex = startIndecies[i];

            for (let j = 0; j < 8; j++) {

                let index = startIndex + j;

                // No piece or opponents piece
                let piece = state.board[index];
                if (piece == null || piece.color !== turnsColor) continue;

                // Defines the search area of the board.
                let pieceNotVisible = !inViewInfo[index];
                if (pieceNotVisible) continue;

                if (piece.type === Darkchess.PAWN) {

                    let square = index + Darkchess.PAWN_OFFSETS[turnsColor][0];

                    let movesToTheDark = !inViewInfo[square];
                    if (movesToTheDark) continue;

                    if (!captureOnly) {

                        let blockingPiece = state.board[square];
                        if (blockingPiece == null) {

                            // Single pawn move
                            addMove(turnsColor, moves, piece.type, index, square, Darkchess.BITS.NORMAL);

                            // Double pawn move
                            square = index + Darkchess.PAWN_OFFSETS[turnsColor][1];

                            let movesToTheDark = !inViewInfo[square];
                            if (!movesToTheDark) {
                                let blockingPiece2 = state.board[square];

                                if (secondRank[turnsColor] === Darkchess.rank(index) && blockingPiece2 == null) {
                                    addMove(turnsColor, moves, piece.type, index, square, Darkchess.BITS.BIG_PAWN);
                                }
                            }
                        }
                    }

                    // Pawn capture
                    for (let j = 2; j < 4; j++) {

                        let square = index + Darkchess.PAWN_OFFSETS[turnsColor][j];

                        let isOffTheBoard = (square & 0x88);
                        if (isOffTheBoard) continue;

                        let movesToTheDark = !inViewInfo[square];
                        if (movesToTheDark) continue;

                        let blockingPiece3 = state.board[square];
                        if (blockingPiece3 != null && blockingPiece3.color === opponentColor) {
                            addMove(turnsColor, moves, piece.type, index, square, Darkchess.BITS.CAPTURE);
                        }
                    }


                } else {

                    let offsets = Darkchess.PIECE_OFFSETS[piece.type].length;
                    for (let j = 0; j < offsets; ++j) {
                        let offset = Darkchess.PIECE_OFFSETS[piece.type][j];
                        let square = index;

                        while (true) {
                            square += offset;

                            let isOffTheBoard = (square & 0x88);
                            if (isOffTheBoard) break;

                            let movesToTheDark = !inViewInfo[square];
                            if (movesToTheDark) continue;

                            let blockingPiece = state.board[square];
                            if (blockingPiece == null) {

                                if (!captureOnly) {
                                    addMove(turnsColor, moves, piece.type, index, square, Darkchess.BITS.NORMAL);
                                }

                            } else {
                                let isOwnPiece = (blockingPiece.color === turnsColor);
                                if (isOwnPiece) break;

                                addMove(turnsColor, moves, piece.type, index, square, Darkchess.BITS.CAPTURE);

                                break;
                            }

                            if (piece.type === 'n' || piece.type === 'k') break;
                        }
                    }
                }
            }
        }

        if (!captureOnly) {

            // King-side castling
            if (state.castling[turnsColor] & Darkchess.BITS.KSIDE_CASTLE) {

                let castlingFrom = state.kings[turnsColor];
                let castlingTo = castlingFrom + 2;

                if (inViewInfo[castlingFrom] && inViewInfo[castlingTo]) {
                    if (state.board[castlingFrom + 1] == null && state.board[castlingTo] == null) {
                        addMove(turnsColor, moves, Darkchess.KING, state.kings[turnsColor], castlingTo, Darkchess.BITS.KSIDE_CASTLE);
                    }
                }
            }

            // Queen-side castling
            if (state.castling[turnsColor] & Darkchess.BITS.QSIDE_CASTLE) {
                let castlingFrom = state.kings[turnsColor];
                let castlingTo = castlingFrom - 2;

                if (inViewInfo[castlingFrom] && inViewInfo[castlingTo]) {
                    if (state.board[castlingFrom - 1] == null && state.board[castlingFrom - 2] == null && state.board[castlingFrom - 3] == null) {
                        addMove(turnsColor, moves, Darkchess.KING, state.kings[turnsColor], castlingTo, Darkchess.BITS.QSIDE_CASTLE);
                    }
                }
            }
        }

        return moves;
    }

    function getTurn()  {
        return state.turn;
    }

    function getInView(color) {
        return (color !== undefined)? state.inView[color]: state.inView[state.turn];
    }

    function getBoard() {
        return state.board;
    }

    function getSAN(move, color) {
        let knowToSquare = state.inView[color][move.to];
        if (!knowToSquare) {
            return '?';
        }

        let output = '';

        if (move.flags & Darkchess.BITS.CAPTURE) {

            if (move.piece !== Darkchess.PAWN && knowToSquare) {
                output += move.piece.toUpperCase();
            }

        } else {
            if (move.piece !== Darkchess.PAWN) {
                output += move.piece.toUpperCase();
            }
        }

        if (move.flags & Darkchess.BITS.CAPTURE) {
            if (move.piece === Darkchess.PAWN && knowToSquare) {
                output += Darkchess.algebraic(move.from)[0];
            }
            output += 'x';
        }

        output += Darkchess.algebraic(move.to);

        if (move.flags & Darkchess.BITS.PROMOTION) {
            output += '=' + move.promotion.toUpperCase();
        }

        return output;
    }

    function getPiece0x88(square) {
        return state.board[square];
    }

    function generateFen(playerColor) {

        let inView = state.inView[playerColor];

        let darkSquares = 0;
        let empty = 0;
        let fen = '';
        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {
            if (!inView[i]) {
                if (empty > 0) {
                    fen += empty;
                    empty = 0;
                }

                darkSquares++;
            } else if (state.board[i] == null) {
                if (darkSquares > 0) {
                    fen += String.fromCharCode(98 + darkSquares);
                    darkSquares = 0;
                }

                empty++;
            } else {

                if (darkSquares > 0) {
                    fen += String.fromCharCode(98 + darkSquares);
                    darkSquares = 0;

                } else if (empty > 0) {
                    fen += empty;
                    empty = 0;

                }

                let piece = state.board[i].type;
                let color = state.board[i].color;
                fen += (color === Darkchess.WHITE) ?  piece.toUpperCase() :
                                                      piece.toLowerCase();
            }

            if ((i + 1) & 0x88) {

                if (darkSquares > 0) {
                    fen += String.fromCharCode(98 + darkSquares);
                    darkSquares = 0;

                } else if (empty > 0) {
                    fen += empty;
                    empty = 0;

                }

                if (i !== Darkchess.SQUARES.h1) {
                    fen += '/';
                }

                i += 8;
            }
        }

        let castlingFlags = '';
        if (state.castling[Darkchess.WHITE] & Darkchess.BITS.KSIDE_CASTLE) { castlingFlags += 'K'; }
        if (state.castling[Darkchess.WHITE] & Darkchess.BITS.QSIDE_CASTLE) { castlingFlags += 'Q'; }
        if (state.castling[Darkchess.BLACK] & Darkchess.BITS.KSIDE_CASTLE) { castlingFlags += 'k'; }
        if (state.castling[Darkchess.BLACK] & Darkchess.BITS.QSIDE_CASTLE) { castlingFlags += 'q'; }

        // do we have an empty castling flag?
        castlingFlags = castlingFlags || '-';

        return [fen, state.turn, castlingFlags, state.halfMoves, state.moveNumber].join(' ');
    }

    function generateCompleteFen() {
        let empty = 0;
        let piecePlacement = '';

        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {
            let piece = state.board[i];

            if (piece == null) {
                ++empty;
            } else {
                if (empty > 0) {
                    piecePlacement += empty;
                    empty = 0;
                }
                piecePlacement += (piece.color === Darkchess.WHITE) ? piece.type.toUpperCase():
                                                                      piece.type.toLowerCase();
            }

            if ((i + 1) & 0x88) {
                if (empty > 0) {
                    piecePlacement += empty;
                }

                if (i !== Darkchess.SQUARES.h1) {
                    piecePlacement += '/';
                }

                empty = 0;
                i += 8;
            }
        }

        let castlingFlags = '';
        if (state.castling[Darkchess.WHITE] & Darkchess.BITS.KSIDE_CASTLE) { castlingFlags += 'K'; }
        if (state.castling[Darkchess.WHITE] & Darkchess.BITS.QSIDE_CASTLE) { castlingFlags += 'Q'; }
        if (state.castling[Darkchess.BLACK] & Darkchess.BITS.KSIDE_CASTLE) { castlingFlags += 'k'; }
        if (state.castling[Darkchess.BLACK] & Darkchess.BITS.QSIDE_CASTLE) { castlingFlags += 'q'; }

        // Empty castling flag?
        castlingFlags = castlingFlags || '-';

        let fenString = [
            piecePlacement,
            state.turn,
            state.castlingFlags,
            state.halfMoves,
            state.moveNumber
        ].join(' ');

        return fenString;
    }

    //  DEBUG FUNCTIONS:

    function logBoard(color) {
        let useInView = (color !== undefined);
        let inView = (useInView)? state.inView[color]: [];

        console.log('\t+---+---+---+---+---+---+---+---+');

        let row = '';
        for (let i = Darkchess.SQUARES.a8; i <= Darkchess.SQUARES.h1; i++) {

            let isOffTheBoard = (i & 0x88);
            if (isOffTheBoard) { i += 7; continue; }

            let piece = state.board[i];

            let pieceType = ' ';
            if (piece != null) {
                pieceType = (piece.color === Darkchess.WHITE)? piece.type.toUpperCase(): piece.type;
            }

            row += ' ';
            row += (useInView && !inView[i])? 'X': pieceType;
            row += ' |';

            let nextIsOfBoard = ((i+1) & 0x88);
            if (nextIsOfBoard) {
                console.log('\t|' + row);
                row = '';
            }
        }

        console.log('\t+---+---+---+---+---+---+---+---+');
        console.log('\t  A   B   C   D   E   F   G   H  ');
    }

    function saveToPGN(pgnOptions) {

        let newLine = '\n';
        let maxWidth = 0;

        if (typeof options !== 'undefined') {
            if (typeof options.newlineChar === 'string') newLine = options.newlineChar;
            if (typeof options.maxWidth === 'number') maxWidth = options.maxWidth;
        }

        //  NOTE(thomas):
        //  Add header

        //  Reversed history, needs to pop moves in order to examine disambiguate moves
        let reversedHistory = [];
        while (state.history.length > 0) {
            reversedHistory.push(undoMove());
        }

        let moves = [];
        let moveString = '';

        while (reversedHistory.length > 0) {
            let moveObject = reversedHistory.pop();

            if (!state.history.length && moveObject.color === Darkchess.BLACK) {
                moveString = state.moveNumber + '. ...';
            } else if (moveObject.color === Darkchess.WHITE) {
                if (moveString.length) {
                    moves.push(moveString);
                }

                moveString = state.moveNumber + '.';
            }

            moveString = moveString + ' ' + moveToSan(moveObject);
            move(moveObject);
        }

        //  Any leftover moves?
        if (moveString.length) {
            moves.push(moveString);
        }

        //  TODO(thomas): Add header result if any
        let results = [];

        if (maxWidth === 0) {
            return results.join('') + moves.join(' ');
        }

        // Wrap PGN output at maxWidth
        let currentWidth = 0;
        for (let i = 0; i < moves.length; i++) {
            let move = moves[i];

            // If current move fill push past max width
            if (currentWidth + move.length > maxWidth && i !== 0) {

                // Do not end the line with whitespace
                if (result[result.length - 1] === ' ') {
                    results.pop();
                }

                results.push(newline);
                currentWidth = 0;
            } else if (i !== 0) {
                results.push(' ');
                currentWidth++;
            }

            results.push(move);
            currentWidth += move.length;
        }

        return results.join('');
    }

    let API = {

        move: move,
        undoMove: undoMove,
        swapPlayer: swapPlayer,
        setState: setState,
        saveToPGN: saveToPGN,

        // Getters
        getWinner: getWinner,
        getMoves: generateMoves,
        getTurn: getTurn,
        getInView: getInView,
        getBoard: getBoard,
        getSAN: getSAN,
        getPiece: getPiece0x88,
        getFen: generateCompleteFen,
        getDarkFen: generateFen,

        //  Boolean getters
        isValidMove: isValidMove,
        isGameOver: isGameOver,
        isDraw: inDraw,
        hit50MoveRule: hit50MoveRule,
        isInsufficientMaterial: insufficientMaterial,

        //  Debug functions
        logBoard: logBoard,
    };

    return API;
}

// "STATIC" PROPERTIE & METHODS:

Darkchess.DEFAULT_POSITION = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq 0 1';

Darkchess.BLACK = 'b';
Darkchess.WHITE = 'w';

Darkchess.PAWN = 'p';
Darkchess.KNIGHT = 'n';
Darkchess.BISHOP = 'b';
Darkchess.ROOK = 'r';
Darkchess.QUEEN = 'q';
Darkchess.KING = 'k';

Darkchess.LIGHT = 'light';
Darkchess.DARK = 'dark';

Darkchess.VISIBLE = 'visible';
Darkchess.HIDDEN = 'hidden';

Darkchess.BITS = {
    NORMAL: 1,
    CAPTURE: 2,
    BIG_PAWN: 4,
    EP_CAPTURE: 8,
    PROMOTION: 16,
    KSIDE_CASTLE: 32,
    QSIDE_CASTLE: 64
};

Darkchess.RANK_1 = 7;
Darkchess.RANK_2 = 6;
Darkchess.RANK_3 = 5;
Darkchess.RANK_4 = 4;
Darkchess.RANK_5 = 3;
Darkchess.RANK_6 = 2;
Darkchess.RANK_7 = 1;
Darkchess.RANK_8 = 0;

Darkchess.SQUARES = {
    a8:   0, b8:   1, c8:   2, d8:   3, e8:   4, f8:   5, g8:   6, h8:   7,
    a7:  16, b7:  17, c7:  18, d7:  19, e7:  20, f7:  21, g7:  22, h7:  23,
    a6:  32, b6:  33, c6:  34, d6:  35, e6:  36, f6:  37, g6:  38, h6:  39,
    a5:  48, b5:  49, c5:  50, d5:  51, e5:  52, f5:  53, g5:  54, h5:  55,
    a4:  64, b4:  65, c4:  66, d4:  67, e4:  68, f4:  69, g4:  70, h4:  71,
    a3:  80, b3:  81, c3:  82, d3:  83, e3:  84, f3:  85, g3:  86, h3:  87,
    a2:  96, b2:  97, c2:  98, d2:  99, e2: 100, f2: 101, g2: 102, h2: 103,
    a1: 112, b1: 113, c1: 114, d1: 115, e1: 116, f1: 117, g1: 118, h1: 119
};

Darkchess.PAWN_OFFSETS = {
    b: [16, 32, 17, 15],
    w: [-16, -32, -17, -15]
};

Darkchess.PIECE_OFFSETS = {
    n: [-18, -33, -31, -14,  18, 33, 31,  14],
    b: [-17, -15,  17,  15],
    r: [-16,   1,  16,  -1],
    q: [-17, -16, -15,   1,  17, 16, 15,  -1],
    k: [-17, -16, -15,   1,  17, 16, 15,  -1]
};

Darkchess.ROOKS = {
    w: [{square: Darkchess.SQUARES.a1, flag: Darkchess.BITS.QSIDE_CASTLE},
        {square: Darkchess.SQUARES.h1, flag: Darkchess.BITS.KSIDE_CASTLE}],
    b: [{square: Darkchess.SQUARES.a8, flag: Darkchess.BITS.QSIDE_CASTLE},
        {square: Darkchess.SQUARES.h8, flag: Darkchess.BITS.KSIDE_CASTLE}]
};

Darkchess.getSAN = function(move) {
    let output = '';

    if (move.flags & Darkchess.BITS.KSIDE_CASTLE) {
        output = 'O-O';
    } else if (move.flags & Darkchess.BITS.QSIDE_CASTLE) {
        output = 'O-O-O';
    } else {

        if (move.piece !== Darkchess.PAWN) {
            output += move.piece.toUpperCase();
        }

        if (move.flags & Darkchess.BITS.CAPTURE) {
            if (move.piece === Darkchess.PAWN) {
                output += Darkchess.algebraic(move.from)[0];
            }
            output += 'x';
        }

        output += Darkchess.algebraic(move.to);

        if (move.flags & Darkchess.BITS.PROMOTION) {
            output += '=' + move.promotion.toUpperCase();
        }
    }

    return output;
}

Darkchess.getColorWord = function(color) {
    return (color === Darkchess.WHITE)? 'White': 'Black';
}

Darkchess.swapColor = function(color) {
    return (color === Darkchess.WHITE)? Darkchess.BLACK : Darkchess.WHITE;
}

Darkchess.squareColor = function(squareIndex) {
    let rank = Darkchess.rank(squareIndex);
    let file = Darkchess.file(squareIndex);
    return ((rank + file) % 2 === 0) ? Darkchess.LIGHT : Darkchess.DARK;
}

Darkchess.rank = function(squareIndex) {
    return squareIndex >> 4;
}

Darkchess.file = function(squareIndex) {
    return squareIndex & 15;
}

Darkchess.isDigit = function(character) {
    return '0123456789'.indexOf(character) !== -1;
}

Darkchess.algebraic = function(squareIndex) {
    let file = Darkchess.file(squareIndex);
    let rank = Darkchess.rank(squareIndex);
    return 'abcdefgh'.substring(file, file + 1) + '87654321'.substring(rank, rank + 1);
}

Darkchess.logMoves = function(moves) {
    let result = '';
    for (let i = 0; i < moves.length; i++) {
        let move = moves[i];
        result += Darkchess.getSAN(move) + ' ';
    }

    return result;
}

module.exports = Darkchess;
