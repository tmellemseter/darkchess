# Darkchess

## Installation

Include this library by running `npm install --save darkchess`. Once installed and required, simply create a new Darkchess object.

```javascript

const Darkchess = require('../node_modules/darkchess/darkchess');

let referee = new Darkchess();
referee.setState(Darkchess.DEFAULT_POSITION);

```

## Features

+ Pass simplified move object
+ Access full move object for more control
+ Validate draw condition (50 move rule or insufficient material)
+ Retrieve dark FEN and dark SAN
+ Provides "static" properties, objects and methods!
+ Depends on NO other libraries!

## Darkchess API
A client-side library is needed to provide a chess board that can interpret [dark fen]. Over at [darkchessDemo], a modified chessboard.js library is provided in order to demonstrate darkchess. _This_ library originated as a clone, but is now __heavily__ based on [chess.js].

Visit the [Wiki] for the documentation.

[Wiki]:             https://gitlab.com/tmellemseter/darkchess/wikis/home
[chess.js]:         https://github.com/jhlywa/chess.js/
[darkchessDemo]:    https://gitlab.com/tmellemseter/darkchessDemo
[dark fen]:         https://gitlab.com/tmellemseter/darkchess/wikis/Home#fen-and-dark-fen
